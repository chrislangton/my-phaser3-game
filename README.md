# Game development with Phaser3

Based on [chrisdlangton/docker-phaser](https://github.com/chrisdlangton/docker-phaser) repo, find it on Docker Hub [chrisdlangton/docker-phaser](https://hub.docker.com/r/chrisdlangton/docker-phaser/)

## Dev environment

1. Install docker

2. run `docker login`

3. Helper functions for phaser docker

```bash
source phaser.bash
```

4. create a `.env` file in the project root, [example here](https://github.com/chrisdlangton/docker-phaser#project-structure)

5. Run the phaser project builder

```bash
phaser-start
```

6. visit [http://localhost:3000/](http://localhost:3000/)

## Helpers

Run phaser container build helper and restart phaser

```bash
phaser-build
```

or

```bash
phaser-rebuild
```

Build for prod

```bash
phaser-build production
```

Stop Phaser

```bash
phaser-stop
```

Restart phaser

```bash
phaser-restart
```

Access a shell in the docker container

```bash
phaser-exec bash
```

or execute inside the container from the host

```bash
phaser-exec npm run build
```
