import Phaser from 'phaser';
import FSXHR from './src/fs-xml-http-request';

// Monkey-patches LoaderPlugin to use fs.readFile() when it tries to perform XHR
FSXHR.install(Phaser.Loader.LoaderPlugin.prototype, 'processLoadQueue')

const game = new Phaser.Game({
  scene: {
    preload() {
      this.load.image('foo', '/mnt/e/linux-game-jam-2018')
    }
  }
})